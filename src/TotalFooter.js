/*
 * Backgrid.js Totals Footer
 *
 * Copyright (c) 2014 Base79
 * Licensed under the MIT license.
 * https://bitbucket.org:base79/backgridjs-total-footer/LICENSE-MIT
 */

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['underscore', 'backbone', 'backgrid'],
            function (_, Backbone, Backgrid) {
                return (factory(_, Backbone, Backgrid));
            }
        );
    }
    else if (typeof exports === 'object') {
        module.exports = factory(
            require('underscore'),
            require('backbone'),
            require('backgrid')
        );
    }
    else {
        factory(root._, root.Backbone, root.Backgrid);
    }
} (this, function (_, Backbone, Backgrid) {
    'use strict';

    var TotalFooterCell = Backbone.View.extend({
        tagName: 'td',
        initialize: function (options) {
            this.options = options;

            this.column = this.options.column;
            if (!(this.column instanceof Backgrid.Column)) {
                this.column = new Backgrid.Column(this.column);
            }

            this.collection = this.options.collection;

            this.cellType = this.column.get('cell').prototype.className;
            this.colName = this.column.get('name');
            this.colIndex = this.column.get('colIndex');

            this.formatter = this._getFormatter();

            Backbone.View.prototype.initialize.apply(this, arguments);
        },
        _getFormatter: function () {
            var cellFormatter = this.column.get('cell').prototype.formatter || 'Cell';

            var formatter = Backgrid.resolveNameToClass(cellFormatter, "Formatter");

            if (!_.isFunction(formatter.fromRaw) && !_.isFunction(formatter.toRaw)) {
                /*jshint newcap: false */
                formatter = new formatter();
            }

            return formatter;
        },
        render: function () {
            this.$el.empty();
            var value = document.createTextNode(this.getValue());
            this.$el.append(value);
            this.$el.addClass(this.cellType);
            this.$el.addClass(this.colName);
            this.delegateEvents();
            return this;
        },
        getValue: function () {
            if (!_.isUndefined(this.options.firstColumnValue) && this._isFirstColumn()) {
                return this.options.firstColumnValue;
            }

            switch (this.cellType) {
                case 'number-cell':
                    return this.formatter.fromRaw(this._sumColumn());
                case 'integer-cell':
                    this.formatter.decimals = 0;
                    return this.formatter.fromRaw(this._sumColumn());
                default:
                    return '';
            }
        },
        _isFirstColumn: function () {
            return this.colIndex === 0;
        },
        _sumColumn: function () {
            return this.collection.reduce(function (result, model) {
                return result + model.get(this.colName);
            }, 0, this);
        }
    });

    var TotalFooterRow = Backgrid.Row.extend({
        initialize: function (options) {
            this.firstColumnValue = options.firstColumnValue;
            Backgrid.Row.prototype.initialize.apply(this, arguments);
        },
        makeCell: function (column, options) {
            column.set('colIndex', _.indexOf(options.columns.models, column));
            var footerCell = column.get('footerCell') || options.footerCell || TotalFooterCell;

            /*jshint newcap: false */
            return new footerCell({
                column: column,
                collection: this.collection,
                firstColumnValue: this.firstColumnValue
            });
        }
    });

    var TotalFooter = Backgrid.Footer.extend({
        initialize: function (options) {
            Backgrid.Footer.prototype.initialize.apply(this, arguments);
            this.row = new TotalFooterRow({
                columns: this.columns,
                collection: this.collection,
                firstColumnValue: this.firstColumnValue
            });
        },
        render: function () {
            this.$el.append(this.row.render().$el);
            this.delegateEvents();
            return this;
        }
    });

    return _.extend(Backgrid.Extension, {
        TotalFooter: {
            Cell: TotalFooterCell,
            Row: TotalFooterRow,
            Footer: TotalFooter
        }
    });
}));
